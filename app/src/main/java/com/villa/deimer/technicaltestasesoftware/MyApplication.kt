package com.villa.deimer.technicaltestasesoftware

import android.app.Application
import android.content.Context
import com.villa.deimer.technicaltestasesoftware.utils.RUtil.Companion.rString
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        setupRealmConfiguration()
    }

    companion object {
        private lateinit var instance: MyApplication

        fun getInstance(): MyApplication {
            return instance
        }

        fun getAppContext(): Context {
            return instance.applicationContext
        }
    }

    private fun setupRealmConfiguration() {
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .name(rString(R.string.app_name))
            .schemaVersion(0)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }
}