package com.villa.deimer.technicaltestasesoftware.data.db

import com.villa.deimer.technicaltestasesoftware.data.entities.ProductEntity
import com.villa.deimer.technicaltestasesoftware.di.database.DaggerIDatabaseComponent
import com.villa.deimer.technicaltestasesoftware.di.database.DatabaseModule
import io.reactivex.Observable
import io.realm.Realm

class DatabaseHelper {

    private val realmInstance = Realm.getDefaultInstance()

    init {
        DaggerIDatabaseComponent.builder().databaseModule(DatabaseModule()).build().inject(this)
    }

    fun saveProducts(entities: List<ProductEntity>): Observable<Boolean> {
        realmInstance.executeTransaction { realm ->
            realm.insertOrUpdate(entities)
        }
        return Observable.just(true)
    }

    fun getProducts(): Observable<List<ProductEntity>>? {
        val entities = realmInstance.where(ProductEntity::class.java).findAll()
        return Observable.just(entities)
    }

    fun findProduct(productId: Int): Observable<ProductEntity> {
        val product = realmInstance.where(ProductEntity::class.java)
            .equalTo("id", productId)
            .findFirst()
        return Observable.just(product)
    }
}