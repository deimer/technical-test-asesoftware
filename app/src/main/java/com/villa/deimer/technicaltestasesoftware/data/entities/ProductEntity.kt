package com.villa.deimer.technicaltestasesoftware.data.entities

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ProductEntity: RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var name: String? = null
    var description: String? = null
    var price: Float? = null
    var image: String? = null
}