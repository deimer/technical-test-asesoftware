package com.villa.deimer.technicaltestasesoftware.data.interactors

import com.villa.deimer.technicaltestasesoftware.data.entities.ProductEntity
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.data.repositories.products.IProductRepository
import com.villa.deimer.technicaltestasesoftware.di.product.DaggerIProductComponent
import com.villa.deimer.technicaltestasesoftware.di.product.ProductModule
import io.reactivex.Observable
import javax.inject.Inject

class ProductInteractor {

    @Inject
    lateinit var productRepository: IProductRepository

    init {
        DaggerIProductComponent.builder().productModule(ProductModule()).build().inject(this)
    }

    fun find(productId: Int): Observable<ProductModel>? {
        return productRepository.find(productId)?.flatMap { entity ->
            Observable.just(convertProductEntityToModel(entity))
        }
    }

    fun getAll(): Observable<List<ProductModel>>? {
        return productRepository.getAll()?.flatMap { entities ->
            Observable.just(convertProductsEntityToModels(entities))
        }
    }

    fun createAll(models: List<ProductModel>): Observable<Boolean>? {
        return productRepository.createAll(convertProductsModelToEntity(models))
    }

    private fun convertProductEntityToModel(entity: ProductEntity): ProductModel {
        return ProductModel().apply {
            id = entity.id
            name = entity.name
            description = entity.description
            price = entity.price
            image = entity.image
        }
    }

    private fun convertProductsModelToEntity(models: List<ProductModel>): List<ProductEntity> {
        val entities = mutableListOf<ProductEntity>()
        models.forEach { model ->
            val entity = ProductEntity().apply {
                id = model.id
                name = model.name
                description = model.description
                price = model.price
                image = model.image
            }
            entities.add(entity)
        }
        return entities
    }

    private fun convertProductsEntityToModels(entities: List<ProductEntity>?): List<ProductModel> {
        val models = mutableListOf<ProductModel>()
        entities?.forEach { entity ->
            val model = ProductModel().apply {
                id = entity.id
                name = entity.name
                description = entity.description
                price = entity.price
                image = entity.image
            }
            models.add(model)
        }
        return models
    }
}