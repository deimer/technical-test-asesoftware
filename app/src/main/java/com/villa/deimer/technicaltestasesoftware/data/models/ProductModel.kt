package com.villa.deimer.technicaltestasesoftware.data.models

class ProductModel {
    var id: Int? = null
    var name: String? = null
    var description: String? = null
    var price: Float? = null
    var image: String? = null
}