package com.villa.deimer.technicaltestasesoftware.data.repositories.products

import com.villa.deimer.technicaltestasesoftware.data.entities.ProductEntity
import io.reactivex.Observable

interface IProductRepository {

    fun createAll(entities: List<ProductEntity>): Observable<Boolean>?

    fun getAll(): Observable<List<ProductEntity>>?

    fun find(productId: Int): Observable<ProductEntity>?
}