package com.villa.deimer.technicaltestasesoftware.data.repositories.products

import com.villa.deimer.technicaltestasesoftware.data.db.DatabaseHelper
import com.villa.deimer.technicaltestasesoftware.data.entities.ProductEntity
import com.villa.deimer.technicaltestasesoftware.di.product.DaggerIProductComponent
import com.villa.deimer.technicaltestasesoftware.di.product.ProductModule
import io.reactivex.Observable
import javax.inject.Inject

class ProductRepository: IProductRepository {

    @Inject
    lateinit var databaseHelper: DatabaseHelper

    init {
        DaggerIProductComponent.builder().productModule(ProductModule()).build().inject(this)
    }

    override fun createAll(entities: List<ProductEntity>): Observable<Boolean>? {
        return databaseHelper.saveProducts(entities)
    }

    override fun getAll(): Observable<List<ProductEntity>>? {
        return databaseHelper.getProducts()
    }

    override fun find(productId: Int): Observable<ProductEntity>? {
        return databaseHelper.findProduct(productId)
    }
}