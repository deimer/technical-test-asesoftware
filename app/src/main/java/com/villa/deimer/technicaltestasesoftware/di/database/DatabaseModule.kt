package com.villa.deimer.technicaltestasesoftware.di.database

import com.villa.deimer.technicaltestasesoftware.data.db.DatabaseHelper
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule {

    @Provides
    fun provideDatabaseHelper(): DatabaseHelper {
        return DatabaseHelper()
    }
}