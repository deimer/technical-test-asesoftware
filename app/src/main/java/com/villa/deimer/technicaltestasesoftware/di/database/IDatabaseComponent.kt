package com.villa.deimer.technicaltestasesoftware.di.database

import com.villa.deimer.technicaltestasesoftware.data.db.DatabaseHelper
import com.villa.deimer.technicaltestasesoftware.data.repositories.products.ProductRepository
import dagger.Component

@Component(modules = [DatabaseModule::class])
interface IDatabaseComponent {

    fun inject(databaseHelper: DatabaseHelper)
    fun inject(productRepository: ProductRepository)
}