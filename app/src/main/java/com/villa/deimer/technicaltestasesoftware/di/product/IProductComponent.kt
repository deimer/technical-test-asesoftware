package com.villa.deimer.technicaltestasesoftware.di.product

import com.villa.deimer.technicaltestasesoftware.data.interactors.ProductInteractor
import com.villa.deimer.technicaltestasesoftware.data.repositories.products.ProductRepository
import com.villa.deimer.technicaltestasesoftware.views.details.DetailViewModel
import com.villa.deimer.technicaltestasesoftware.views.timeline.TimelineViewModel
import com.villa.deimer.technicaltestasesoftware.views.welcome.WelcomeViewModel
import dagger.Component

@Component(modules = [ProductModule::class])
interface IProductComponent {

    fun inject(productRepository: ProductRepository)
    fun inject(productInteractor: ProductInteractor)

    fun inject(welcomeViewModel: WelcomeViewModel)
    fun inject(timelineViewModel: TimelineViewModel)
    fun inject(detailViewModel: DetailViewModel)
}