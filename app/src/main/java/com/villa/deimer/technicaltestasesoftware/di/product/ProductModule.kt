package com.villa.deimer.technicaltestasesoftware.di.product

import com.villa.deimer.technicaltestasesoftware.data.db.DatabaseHelper
import com.villa.deimer.technicaltestasesoftware.data.interactors.ProductInteractor
import com.villa.deimer.technicaltestasesoftware.data.repositories.products.IProductRepository
import com.villa.deimer.technicaltestasesoftware.data.repositories.products.ProductRepository
import dagger.Module
import dagger.Provides

@Module
class ProductModule {

    @Provides
    fun provideProductRepository(): IProductRepository {
        return ProductRepository()
    }
    @Provides
    fun provideProductInteractor(): ProductInteractor {
        return ProductInteractor()
    }
    @Provides
    fun provideDatabaseHelper(): DatabaseHelper {
        return DatabaseHelper()
    }
}