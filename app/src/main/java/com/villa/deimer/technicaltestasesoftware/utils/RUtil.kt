package com.villa.deimer.technicaltestasesoftware.utils

import com.villa.deimer.technicaltestasesoftware.MyApplication

class RUtil {

    companion object {
        fun rString(resId: Int): String {
            return MyApplication.getInstance().getString(resId)
        }
        fun rArry(resId: Int): List<String> {
            return MyApplication.getInstance().resources.getStringArray(resId).asList()
        }
    }
}