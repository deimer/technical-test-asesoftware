package com.villa.deimer.technicaltestasesoftware.views.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.utils.RUtil.Companion.rString
import com.villa.deimer.technicaltestasesoftware.views.details.DetailActivity
import kotlinx.android.synthetic.main.product_item.view.*

class ProductRecyclerAdapter(
    private val products: List<ProductModel>,
    private val context: Context): RecyclerView.Adapter<ViewHolderProduct>() {

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderProduct {
        return ViewHolderProduct(LayoutInflater.from(context).inflate(R.layout.product_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderProduct, position: Int) {
        val product = products[position]
        holder.lblName.text = "${product.id}. ${product.name}"
        holder.lblPrice.text = "$${product.price?.toInt()}"
        holder.cardItem.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(rString(R.string.product_id), product.id)
            context.startActivity(intent)
        }
        setupImage(holder.imgImage, product.image)
    }

    private fun setupImage(imgImage: ImageView, urlImage: String?) {
        Picasso.get()
            .load(urlImage)
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_foreground)
            .fit()
            .centerCrop()
            .into(imgImage)
    }
}

class ViewHolderProduct (view: View): RecyclerView.ViewHolder(view) {
    val lblName: TextView = view.lblName
    val lblPrice: TextView = view.lblPrice
    val imgImage: ImageView = view.imgImage
    val cardItem: CardView = view.cardItem
}