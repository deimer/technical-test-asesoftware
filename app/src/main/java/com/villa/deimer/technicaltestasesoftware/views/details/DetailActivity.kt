package com.villa.deimer.technicaltestasesoftware.views.details

import android.graphics.PorterDuff
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.utils.RUtil.Companion.rString
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*

class DetailActivity : AppCompatActivity() {

    private lateinit var detailViewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        setupView()
    }

    private fun setupView() {
        title = getString(R.string.title_activity_detail)
        setupToolbar()
        setupViewModel()
        initSubscriptions()
    }

    private fun setupToolbar() {
        toolbar.title = ""
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
            toolbar.contentInsetStartWithNavigation = 0
            toolbar.setNavigationOnClickListener { finish() }
            toolbar.navigationIcon?.setColorFilter(
                resources.getColor(R.color.primaryText), PorterDuff.Mode.SRC_ATOP
            )
        }
    }

    private fun setupViewModel() {
        val productId = intent.getIntExtra(rString(R.string.product_id), 0)
        detailViewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        detailViewModel.findProduct(productId)
    }

    private fun initSubscriptions() {
        detailViewModel.singleLiveEvent.observe(this, Observer {
            when (it) {
                is DetailViewModel.ViewEvent.ResponseSuccess -> {
                    setupInfoProduct(it.product)
                }
                is DetailViewModel.ViewEvent.ResponseError -> {
                    Snackbar.make(lblDescription, it.errorMessage, Snackbar.LENGTH_LONG).show()
                    println("Error: ${it.errorMessage}")
                }
            }
        })
    }

    private fun setupInfoProduct(product: ProductModel) {
        lblName.text = "Nombre: ${product.name}"
        lblPrice.text = "Precio: $${product.price?.toInt()}"
        lblDescription.text = "Descripción: ${product.description}"
        Picasso.get()
            .load(rString(R.string.image_product))
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_foreground)
            .fit()
            .centerCrop()
            .into(imgImage)
    }
}
