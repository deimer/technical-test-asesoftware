package com.villa.deimer.technicaltestasesoftware.views.details

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.data.interactors.ProductInteractor
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.di.product.DaggerIProductComponent
import com.villa.deimer.technicaltestasesoftware.di.product.ProductModule
import com.villa.deimer.technicaltestasesoftware.livedata.SingleLiveEvent
import com.villa.deimer.technicaltestasesoftware.utils.RUtil
import javax.inject.Inject

@SuppressLint("CheckResult")
class DetailViewModel: ViewModel() {

    @Inject
    lateinit var productInteractor: ProductInteractor

    init {
        DaggerIProductComponent.builder().productModule(ProductModule()).build().inject(this)
    }

    var singleLiveEvent: SingleLiveEvent<ViewEvent> = SingleLiveEvent()

    sealed class ViewEvent {
        class ResponseError (val errorMessage: String): ViewEvent()
        class ResponseSuccess(val product: ProductModel): ViewEvent()
    }

    fun findProduct(productId: Int) {
        productInteractor.find(productId)?.subscribe({
            singleLiveEvent.value = ViewEvent.ResponseSuccess(it)
        }, {
            singleLiveEvent.value = ViewEvent.ResponseError(it.message.toString())
        })
    }
}