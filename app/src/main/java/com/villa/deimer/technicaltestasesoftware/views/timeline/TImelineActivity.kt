package com.villa.deimer.technicaltestasesoftware.views.timeline

import android.os.Bundle
import android.app.AlertDialog
import android.view.View
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.views.adapters.ProductRecyclerAdapter
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_timeline.*
import kotlinx.android.synthetic.main.content_timeline.*

class TImelineActivity : AppCompatActivity() {

    private lateinit var timelineViewModel: TimelineViewModel
    private lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timeline)
        setSupportActionBar(toolbar)
        setupView()
    }

    private fun setupView() {
        title = getString(R.string.title_activity_timeline)
        setupDialogProgress()
        setupViewModel()
        initSubscriptions()
    }

    private fun setupDialogProgress() {
        dialog = SpotsDialog.Builder()
            .setContext(this)
            .setMessage(getString(R.string.get_data_message))
            .setCancelable(false)
            .build()
            .apply { show() }
    }

    private fun setupViewModel() {
        timelineViewModel = ViewModelProviders.of(this).get(TimelineViewModel::class.java)
        timelineViewModel.getProducts()
    }

    private fun initSubscriptions() {
        timelineViewModel.singleLiveEvent.observe(this, Observer {
            when (it) {
                is TimelineViewModel.ViewEvent.ResponseSuccess -> {
                    dialog.dismiss()
                    setupRecyclerProducts(it.products)
                }
                is TimelineViewModel.ViewEvent.ResponseError -> {
                    dialog.dismiss()
                    Snackbar.make(recyclerProducts, it.errorMessage, Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun setupRecyclerProducts(products: List<ProductModel>) {
        val adapter = ProductRecyclerAdapter(products, this)
        val staggeredGridLayoutManager = LinearLayoutManager(this)
        recyclerProducts.layoutManager = staggeredGridLayoutManager
        recyclerProducts.adapter = adapter
        recyclerProducts.visibility = View.VISIBLE
    }
}
