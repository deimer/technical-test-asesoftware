package com.villa.deimer.technicaltestasesoftware.views.timeline

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.data.interactors.ProductInteractor
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.di.product.DaggerIProductComponent
import com.villa.deimer.technicaltestasesoftware.di.product.ProductModule
import com.villa.deimer.technicaltestasesoftware.livedata.SingleLiveEvent
import com.villa.deimer.technicaltestasesoftware.utils.RUtil.Companion.rString
import javax.inject.Inject

@SuppressLint("CheckResult")
class TimelineViewModel: ViewModel() {

    @Inject
    lateinit var productInteractor: ProductInteractor

    init {
        DaggerIProductComponent.builder().productModule(ProductModule()).build().inject(this)
    }

    var singleLiveEvent: SingleLiveEvent<ViewEvent> = SingleLiveEvent()

    sealed class ViewEvent {
        class ResponseError (val errorMessage: String): ViewEvent()
        class ResponseSuccess(val products: List<ProductModel>): ViewEvent()
    }

    fun getProducts() {
        productInteractor.getAll()?.subscribe({
            if(it.isNotEmpty()) {
                singleLiveEvent.value = ViewEvent.ResponseSuccess(it)
            } else {
                singleLiveEvent.value = ViewEvent.ResponseError(rString(R.string.list_is_empty))
            }
        }, {
            singleLiveEvent.value = ViewEvent.ResponseError(it.message.toString())
        })
    }
}