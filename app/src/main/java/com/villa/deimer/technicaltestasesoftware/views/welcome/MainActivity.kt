package com.villa.deimer.technicaltestasesoftware.views.welcome

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.views.timeline.TImelineActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeViewModel: WelcomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
    }

    private fun setupView() {
        setupViewModel()
        initSubscriptions()
    }

    private fun setupViewModel() {
        welcomeViewModel = ViewModelProviders.of(this).get(WelcomeViewModel::class.java)
        welcomeViewModel.isProductsFull()
    }

    private fun initSubscriptions() {
        welcomeViewModel.singleLiveEvent.observe(this, Observer {
            when(it) {
                is WelcomeViewModel.ViewEvent.ResponseProductsFull -> {
                    if(it.success) {
                        animateLogo()
                    } else {
                        welcomeViewModel.saveProducts()
                    }
                }
                is WelcomeViewModel.ViewEvent.ResponseSaveSuccess -> {
                    if(it.success) {
                        animateLogo()
                    }
                }
                is WelcomeViewModel.ViewEvent.ResponseError -> {
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_LONG).show()
                }
                is WelcomeViewModel.ViewEvent.ActivityTransaction -> {
                    openActivityTimeline()
                }
            }
        })
    }

    private fun animateLogo() {
        YoYo.with(Techniques.SlideInUp)
            .duration(700)
            .withListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    animateLogoPulse()
                    welcomeViewModel.launchActivityTransaction(1000)
                }
            }).playOn(imgLogo)
        imgLogo.visibility = View.VISIBLE
    }

    private fun animateLogoPulse() {
        YoYo.with(Techniques.Tada)
            .duration(700)
            .repeat(700)
            .playOn(imgLogo)
    }

    private fun openActivityTimeline() {
        startActivity(Intent(this, TImelineActivity::class.java))
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}
