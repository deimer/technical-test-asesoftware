package com.villa.deimer.technicaltestasesoftware.views.welcome

import android.annotation.SuppressLint
import android.os.Handler
import androidx.lifecycle.ViewModel
import com.villa.deimer.technicaltestasesoftware.R
import com.villa.deimer.technicaltestasesoftware.data.interactors.ProductInteractor
import com.villa.deimer.technicaltestasesoftware.data.models.ProductModel
import com.villa.deimer.technicaltestasesoftware.di.product.DaggerIProductComponent
import com.villa.deimer.technicaltestasesoftware.di.product.ProductModule
import com.villa.deimer.technicaltestasesoftware.livedata.SingleLiveEvent
import com.villa.deimer.technicaltestasesoftware.utils.RUtil.Companion.rArry
import com.villa.deimer.technicaltestasesoftware.utils.RUtil.Companion.rString
import javax.inject.Inject

@SuppressLint("CheckResult")
class WelcomeViewModel: ViewModel() {

    @Inject
    lateinit var productInteractor: ProductInteractor

    init {
        DaggerIProductComponent.builder().productModule(ProductModule()).build().inject(this)
    }

    var singleLiveEvent: SingleLiveEvent<ViewEvent> = SingleLiveEvent()

    sealed class ViewEvent {
        class ResponseError (val errorMessage: String): ViewEvent()
        class ResponseProductsFull (val success: Boolean): ViewEvent()
        class ResponseSaveSuccess (val success: Boolean): ViewEvent()
        object ActivityTransaction : ViewEvent()
    }

    fun launchActivityTransaction(timeMs: Long) {
        Handler().postDelayed({
            singleLiveEvent.value = ViewEvent.ActivityTransaction
        }, timeMs)
    }

    fun isProductsFull() {
        productInteractor.getAll()?.subscribe({
            if(it.isNotEmpty()) {
                singleLiveEvent.value = ViewEvent.ResponseProductsFull(true)
            } else {
                singleLiveEvent.value = ViewEvent.ResponseProductsFull(false)
            }
        }, {
            singleLiveEvent.value = ViewEvent.ResponseError(it.message.toString())
        })
    }

    fun saveProducts() {
        val products = generateListProducts()
        productInteractor.createAll(products)?.subscribe({
            singleLiveEvent.value = ViewEvent.ResponseSaveSuccess(it)
        }, {
            singleLiveEvent.value = ViewEvent.ResponseError(it.message.toString())
        })
    }

    private fun generateListProducts(): List<ProductModel> {
        val values = rArry(R.array.string_array_values)
        val prices = rArry(R.array.string_array_prices)
        val models = mutableListOf<ProductModel>()
        values.forEachIndexed { index, value ->
            val model = ProductModel().apply {
                id = index + 1
                name = value
                description = rString(R.string.large_text)
                price = prices[index].toFloat()
                image = rString(R.string.image_product)
            }
            models.add(model)
        }
        return models
    }
}